# Brainfuck
## General
Brainfuck is an esoteric language. ... TODO
## Commands

| Command | Meaning |
| ------ | ------ |
| > | the pointer is increased by one |
| < | the pointer is decreased by one |
| + | the value the pointer points at is increased by one |
| - | the value the pointer points at is decreased by one |
| [ | marks the begining of a loop, in case the current value is `0`, the body is skipped |
| ] | marks the end of a loop, in case the current value is not `0`, another iteration is started ( pointer is moved to `[` ) |
| . | the value the pointer points at is printed |
| , | the value the pointer points at is overwritten with the next input |
