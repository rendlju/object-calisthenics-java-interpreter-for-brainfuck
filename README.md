# Java Interpreter for Brainfuck following Object Calisthenics rules
## General
## Object Calisthenics
Object Calisthenics are a set of rules first described by Jeff Bay which will strongly be valued for this project:
1. Use only one level of indentation per method.
2. Don’t use the else keyword.
3. Wrap all primitives and strings.
4. Use only one dot per line.
5. Don’t abbreviate.
6. Keep all entities small.
7. Don’t use any classes with more than two instance variables.
8. Use first-class collections.
9. Don’t use any getters/setters/properties.
